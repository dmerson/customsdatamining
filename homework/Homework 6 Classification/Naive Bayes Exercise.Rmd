---
title: "Naive Bayes"
output: html_notebook
---


```{r}
#Getting started with Naive Bayes
#Install the package
install.packages("e1071")
#Loading the library


library(e1071)


```

```{r}
?naiveBayes #The documentation also contains an example implementation of Titanic dataset
#Next load the Titanic dataset

```

```{r}
data("Titanic")
#Save into a data frame and view it
Titanic_df=as.data.frame(Titanic)
#Creating data from table
repeating_sequence=rep.int(seq_len(nrow(Titanic_df)), Titanic_df$Freq) 
##This will repeat each combination equal to the frequency of each combination
repeating_sequence
head(Titanic_df)
```

```{r}
#Create the dataset by row repetition created
Titanic_dataset=Titanic_df[repeating_sequence,]
#We no longer need the frequency, drop the feature
Titanic_dataset$Freq=NULL
 
```

```{r}
#Fitting the Naive Bayes model
Naive_Bayes_Model=naiveBayes(Survived ~., data=Titanic_dataset)
#What does the model say? Print the model summary
Naive_Bayes_Model
```

```{r} 
#Prediction on the dataset
NB_Predictions=predict(Naive_Bayes_Model,Titanic_dataset)
#Confusion matrix to check accuracy
table(NB_Predictions,Titanic_dataset$Survived)
```

```{r}
#Getting started with Naive Bayes in mlr
#Install the package
#install.packages("mlr")
#Loading the library
library(mlr)
```



```{r}
#Create a classification task for learning on Titanic Dataset and specify the target feature
task = makeClassifTask(data = Titanic_dataset, target = "Survived")
task

```

```{r}
#Initialize the Naive Bayes classifier
selected_model = makeLearner("classif.naiveBayes")

```

```{r}
#Train the model
NB_mlr = train(selected_model, task)
```

```{r}
#Read the model learned  
NB_mlr$learner.model
```

```{r}
#Predict on the dataset without passing the target feature
predictions_mlr = as.data.frame(predict(NB_mlr, newdata = Titanic_dataset[,1:3]))
 
```

```{r}
##Confusion matrix to check accuracy
table(predictions_mlr[,1],Titanic_dataset$Survived)
```


 
