Will I receive a full notebook for approps hearings tonight?
I'd like a call this afternoon or evening to go over the draft Af-Pak regional strategy. Who should be on call? It is quite
good but lacks "milestones" and budgets for each section.
Re the required report for Kerry-Lugar, I want to be sure that the language in both our report and this one echo the
same language.
I never called Patricia Espinosa--should I?
Also, have we rec'd a memo from Shaun Woodard for Robinson and McGuinness calls?
There will also be a PC on Af-Pak Tuesday and I want to be sure our report is ready.
