Here are some matters I haven't raised w you but wanted to get your reaction:
Was there any followup to offer to help/advise on Afghan women?
Re your memo about S/GWI priorities for FY 09 and 10--
Will we be announcing the global womens fund and/or other partnerships at UNGA?
Are we closely coordinating w the WH and its Council?
How can we strengthen the AID Women's office?
Are the Congress Members ok w our work and priorities so far? What about the 2010 budget?
Who will be the UN women's voice on the inside and outside?
How will we mark 15th anniv of Cairo?
Let's do a thorough review about CEDAW strategy. Should we ask to be on contract w H to lead our
efforts?
What's followup w Doug Hattaway?
Are you working w Alec Ross? ECA?
Are you working w WHA and Julissa Reynoso on econ issues?
What are we doing w MEPI?
Are you staffed up now?
Did you see Kay Warren when she was in DC for Eric Goosby's swearing in?
