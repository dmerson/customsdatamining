Melanne--Your travels bring back so many wonderful memories of all we've done together over the last 17 years. When
we are both back, we need to find time for discussing the projects--like cook stoves--that we want to do or facilitate.
Until then, safe travels. H
